#!/usr/bin/env python3

import SquareCrosswordGenerator as square_gen
import json
import argparse
from os import path

GENERATED_PUZZLES_FOLDER = '../generated_puzzles/'

def export_to_json(name, square_crossword, puzzle_path):
    file_name = puzzle_path + name + '.json'
    if path.isfile(file_name):
        raise FileExistsError
    json_dictionary = {}
    json_dictionary['rows'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['rows_regexes']]
    json_dictionary['cols'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['cols_regexes']]
    with open(puzzle_path + name + '.json', 'w') as outfile:
        json.dump(json_dictionary, outfile)
    print(json_dictionary)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
            '-d', '--dimensions', type=int, help='Set the size of the puzzle, where the end result is dxd matrix', required=True)
    parser.add_argument(
        '-n', '--name', type=str, help='The name of puzzle', required=True)
    parser.add_argument(
        '--path', type=str, help='Optional: The directory in which to store the puzzle', required=False)

    parsed_args = parser.parse_args()
    dimensions = parsed_args.dimensions
    puzzle_name = parsed_args.name
    puzzle_path = parsed_args.path

    if puzzle_path is None:
        puzzle_path = GENERATED_PUZZLES_FOLDER

    try:
        export_to_json(puzzle_name, square_gen.random_square_crossword(dimensions), puzzle_path)
    except FileExistsError as e:
        print('The puzzle already exists')

