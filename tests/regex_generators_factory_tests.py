import unittest
from regex_generator.RegexGenerators import RegexGeneratorsFactory
from regex_generator.RegexGenerators import NoopRegexGenerator
from regex_generator.RegexGenerators import CharacterSetGenerator
from regex_generator.RegexGenerators import AlternationGenerator


class RegexGeneratorsFactoryTests(unittest.TestCase):
    def setUp(self):
        self.generator_factory = RegexGeneratorsFactory()

    def test_get_generator(self):
        legitimate_generators = [NoopRegexGenerator, CharacterSetGenerator, AlternationGenerator]
        for index in range(3):
            self.assertIs(self.generator_factory.get_generator(index), legitimate_generators[index])


if __name__ == '__main__':
    unittest.main()
