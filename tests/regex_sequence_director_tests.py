import unittest
from regex_generator import RegexSequenceDirector
from regex_generator import RegexGenerators
from regex_generator import RegexSequence


class RegexSequenceDirectorTest(unittest.TestCase):
    def setUp(self):
        self.sequence_director = RegexSequenceDirector.RegexSequenceDirector(3, '',
                                                                             RegexGenerators.RegexGeneratorsFactory())

    def test_generate_regex(self):
        input_noise = {'a': {'positive': ['q', 's', 't'], 'negative': ['z', 'x', 'p']},
                       'b': {'positive': ['w', 'd', 'y'], 'negative': ['v', 'n', 'm']},
                       'c': {'positive': ['e', 'f', 'u'], 'negative': ['j', 'k', 'l']}}

        self.assertIsInstance(self.sequence_director.generate_regex("abc", input_noise), RegexSequence.RegexSequence)


if __name__ == '__main__':
    unittest.main()
