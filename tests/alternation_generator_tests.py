import unittest
from regex_generator.RegexGenerators import AlternationGenerator


class AlternationGeneratorTests(unittest.TestCase):
    def setUp(self):
        self.alternation_generator = AlternationGenerator()

    def test_get_regex(self):
        match_symbols = ["ab", "xy", "45", "2a", "Ab", "df7"]
        reg_exps = ['a|b', "x|y", "4|5", "2|a", "A|b", "d|f|7"]

        for index, match_symbol in enumerate(match_symbols):
            self.assertEqual(self.alternation_generator.get_regex(match_symbol), reg_exps[index])


if __name__ == '__main__':
    unittest.main()
