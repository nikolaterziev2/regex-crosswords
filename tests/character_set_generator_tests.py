import unittest
from regex_generator.RegexGenerators import CharacterSetGenerator


class CharacterSetGeneratorTests(unittest.TestCase):
    def setUp(self):
        self.charset_generator = CharacterSetGenerator()

    def test_get_regex(self):
        match_symbols = ['a', 'b', 'c']
        dont_match_symbols = ['x', 'y', 'z']
        reg_exp_match = '[abc]+'
        reg_exp_dont_match = '[^xyz]+'
        self.assertEqual(self.charset_generator.get_regex(match_symbols, dont_match_symbols, False), reg_exp_match)
        self.assertEqual(self.charset_generator.get_regex(match_symbols, dont_match_symbols, True), reg_exp_dont_match)


if __name__ == '__main__':
    unittest.main()
