import unittest

test_modules = [
    'tests.alternation_generator_tests',
    'tests.character_set_generator_tests',
    'tests.noop_regex_generator_tests',
    'tests.regex_generators_factory_tests',
    'tests.regex_sequence_director_tests'
    ]

suite = unittest.TestSuite()

for t in test_modules:
    try:
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

if __name__ == '__main__':
    unittest.TextTestRunner().run(suite)
