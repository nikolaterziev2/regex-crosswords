import unittest
from regex_generator.RegexGenerators import NoopRegexGenerator


class NoopRegexGeneratorTest(unittest.TestCase):
    def setUp(self):
        self.noop_generator = NoopRegexGenerator()

    def test_get_regex(self):
        match_symbols = ["[^fail$]", "[^2fr$]",
                         "[a|s|d|f]", "[a]+"]
        dont_match_symbols = ["[fdsf]", "[a|g|e|r",
                              "[^hfg$]", "[l?]"]
        for index, element in enumerate(match_symbols):
            self.assertEqual(self.noop_generator.get_regex(element, dont_match_symbols[index]), element)


if __name__ == '__main__':
    unittest.main()
