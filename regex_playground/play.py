#!/usr/bin/python3

import sys, json, re, os
from PyQt4 import QtGui

GENERATED_PUZZLES_FOLDER = '/generated_puzzles'


class RegexLine(QtGui.QLineEdit):
    def __init__( self, parent=None):
        super(RegexLine, self).__init__(parent)
        self.textChanged.connect(self.handleChange)

    def handleChange(self, string):
        if string == '' or string == '\\' or len(string) == 2 and string[0] == '\\':
            return
        self.setText(string[-1])


class RegexCrossword(QtGui.QWidget):
    
    def __init__(self):
        super(RegexCrossword, self).__init__()

        self.layout = QtGui.QGridLayout()
        self.font = QtGui.QFont("Helvetica", 10, QtGui.QFont.Bold, True)

        self.puzzle_path, self.puzzle = '', {}
        self.grid, self.row_regex_texts, self.col_regex_texts = [], [], []

        self.game_status_label = QtGui.QLabel()
        self.restart_button = QtGui.QPushButton('Restart')
        self.restart_button.clicked.connect(self.restart_game)

        self.init_puzzle()
        self.initUI()

    def get_puzzle_path(self):
        cwd = os.getcwd()
        if os.path.isdir(cwd + '/..' + GENERATED_PUZZLES_FOLDER):
            cwd += '/..' + GENERATED_PUZZLES_FOLDER
        path = QtGui.QFileDialog.getOpenFileName(self, "Open file", cwd, "JSON files (*.json)")
        if not os.path.isfile(path):
            exit(1)
        return path

    def reset_variables(self):
        """
        Reset the variables prior the restart of the game
        """
        self.grid, self.row_regex_texts, self.col_regex_texts = [], [], []

    def init_puzzle(self):
        """
        Loads user inputed puzzle, creates all the widgets (lables for the regexes and RegexLine(QLineEdit) for the
        regex input boxes
        """
        self.reset_variables()
        self.puzzle_path = self.get_puzzle_path()
        self.puzzle = RegexCrossword.import_puzzle(self.puzzle_path)

        for row in self.puzzle['rows']:
            self.row_regex_texts.append(QtGui.QLabel(row[1]))
            self.row_regex_texts[-1].setFont(self.font)
            self.grid.append([RegexLine() for _ in range(0, row[0])])

        for col in self.puzzle['cols']:
            # the visual rotation of -90 degrees of the column's text is achived by
            # inserting \n after each symbol
            self.col_regex_texts.append(QtGui.QLabel(RegexCrossword.__transform_col_text(col[1])))
            self.col_regex_texts[-1].setFont(self.font)

    def initUI(self):
        """
        Adds all the created from init_puzzle widgets to the layout, then adds the widgets created in the init function
        """
        self.layout.setSpacing(1)
        row, col = 0, 0
        for regex_row in self.grid:
            self.layout.addWidget(self.row_regex_texts[row], row + 1, col, 1, 1)
            col += 1
            for reg in regex_row:
                self.layout.addWidget(reg, row + 1, col, 1, 1)
                col += 1
            row += 1
            col = 0

        row, col = 0, 1
        for col_text in self.col_regex_texts:
            self.layout.addWidget(col_text, row, col, 1, 1)
            col += 1

        check_status = QtGui.QPushButton('Check')
        check_status.clicked.connect(self.check_puzzle)
        self.layout.addWidget(check_status, len(self.grid) + 1, 0)

        self.game_status_label.setText('')
        self.layout.addWidget(self.game_status_label, len(self.grid) + 1, 1)

        self.layout.addWidget(self.restart_button, len(self.grid) + 1, len(self.grid))

        self.setLayout(self.layout)
        
        self.setGeometry(400, 400, 550, 400)
        self.setWindowTitle('Regex Crossword')
        self.show()

    def clear_layout(self):
        """
        "One liner" that removes all the widgets on our layout
        """
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)

    def restart_game(self):
        """
        Note: Its bound to the restart button
        """
        self.clear_layout()
        self.init_puzzle()
        self.initUI()

    def check_puzzle(self):
        """
        Note: Its bound to the check button
        """
        is_correct = self._check_puzzle_cols() and self._check_puzzle_rows()

        if is_correct:
            self.game_status_label.setText(' Completed!')
            self.game_status_label.setStyleSheet('color: green')
        else:
            self.game_status_label.setText(' Nope')
            self.game_status_label.setStyleSheet('color: red')

    def _check_puzzle_rows(self):
        """
        If all the letters are filled and the regex is matched, will return true
        """
        is_correct = True
        for index, row in enumerate(self.grid):
            word = ''
            is_filled = True
            for col, letter in enumerate(row):
                if not letter.text():
                    is_filled = False
                    break
                word += letter.text()
            if is_filled:
                if not re.search('^' + self.row_regex_texts[index].text() + '$', word):
                    print('Regex {} does not match word {}'. format('^' + self.row_regex_texts[index].text() + '$', word))
                    is_correct = False
            else:
                print('Letter at row,col {},{} is missing'.format(index, col))
                is_correct = False
        return is_correct

    def _check_puzzle_cols(self):
        """
        If all the letters are filled and the regex is matched, will return true
        """
        is_correct = True
        for col in range(0, len(self.grid)):
            word = ''
            is_filled = True
            for row in range(0, len(self.grid)):
                letter = self.grid[row][col]
                if not letter.text():
                    is_filled = False
                    break
                word += letter.text()
            if is_filled:
                # reversing the hack for the visual interpretation of the column regexes via revert_col_text
                # see __translate_col_text for the inverse function
                if not re.search('^' + RegexCrossword.__revert_col_text(self.col_regex_texts[col].text()) + '$', word):
                    print('Regex {} does not match word {}'. format('^' + RegexCrossword.__revert_col_text(self.col_regex_texts[col].text()) + '$', word))
                    is_correct = False
            else:
                print('Letter at row,col {},{} is missing'.format(row, col))
                is_correct = False
        return is_correct

    @staticmethod
    def import_puzzle(path):
        puzzle = {}
        with open(path, 'r') as puzzle:
            puzzle = json.load(puzzle)
        return puzzle

    @staticmethod
    def __transform_col_text(text):
        return '\n'.join(i for i in text)

    @staticmethod
    def __revert_col_text(text):
        return ''.join(text.split('\n'))


def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = RegexCrossword()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
