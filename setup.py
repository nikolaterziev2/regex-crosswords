from distutils.core import setup

setup(
    name='regex-crosswords',
    version='0.1',
    packages=['regex_generator'],
    url='https://gitlab.com/nikolaterziev2/regex-crosswords',
    license='MIT',
    author='vazov',
    author_email='nikolaterziev2@gmail.com',
    description='Generate crosswords. Solve crosswords.',
    install_requires=[
        'PyQt4',
    ]
)
